import React, { lazy, Suspense, useContext, useEffect } from 'react'
import { Route, Switch, useHistory, useRouteMatch } from 'react-router-dom'
import { AuthContext } from '../../shared/contexts/AuthContext'
import { LoadingContext } from '../../shared/contexts/LoadingContext'

const UsersPage = lazy(() => import('../../modules/users-module/UsersPage'))

const UsersLayout = () => {
  const { path } = useRouteMatch()
  const { setLoading } = useContext(LoadingContext)
  const { user } = useContext(AuthContext)
  const history = useHistory()

  useEffect(() => {
    if (!user) {
      history.push('/')
    }
  }, [user])

  const Load = () => {
    useEffect(() => {
      setLoading(true)

      return () => setLoading(false)
    }, [])
    return <></>
  }

  return (
    <Suspense fallback={<Load></Load>}>
      <Switch>
        <Route exact path={path} component={UsersPage} />
      </Switch>
    </Suspense>
  )
}

export default UsersLayout
