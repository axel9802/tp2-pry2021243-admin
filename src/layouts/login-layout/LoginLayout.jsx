import React, { lazy, Suspense, useContext, useEffect } from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import { SCLoader } from '../../shared/components/sc-loader/SCLoader'
import { LoadingContext } from '../../shared/contexts/LoadingContext'

const LoginPage = lazy(() => import('../../modules/login-module/LoginPage'))

const LoginLayout = () => {
  const { path } = useRouteMatch()
  const { setLoading } = useContext(LoadingContext)

  const Load = () => {
    useEffect(() => {
      setLoading(true)

      return () => setLoading(false)
    }, [])
    return <></>
  }

  return (
    <Suspense fallback={<Load></Load>}>
      <Switch>
        <Route exact path={path} component={LoginPage} />
      </Switch>
    </Suspense>
  )
}

export default LoginLayout
