import React, { createContext, useContext } from "react";
import { useJsApiLoader } from "@react-google-maps/api";

const GoogleMapsContext = createContext({  isLoaded: false})
const libraries = ["places"];
export const  GoogleMapsProvider= ({children})=>{
    const {isLoaded} = useJsApiLoader({
        id: "google-map-script",
        googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
        libraries: libraries,
    });
    console.log(process.env.REACT_APP_GOOGLE_MAPS_API_KEY)
    return (
        <GoogleMapsContext.Provider value={{ isLoaded, loadError }}>
          {children}
        </GoogleMapsContext.Provider>
      );
};

export const useGoogleMaps = ()=>useContext(GoogleMapsContext);