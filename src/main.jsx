import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import { LoadingProvider } from './shared/contexts/LoadingContext'
import { AuthProvider } from './shared/contexts/AuthContext'

ReactDOM.render(
  <React.StrictMode>
    <LoadingProvider>
      <AuthProvider>
        <App />
      </AuthProvider>
    </LoadingProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
