import React, { useContext, useState } from 'react'
import { useForm } from 'react-hook-form'
import styles from './LoginPage.module.scss'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { LoadingContext } from '../../shared/contexts/LoadingContext'
import { login } from './endpoints/auth.endpoint'
import { useHistory } from 'react-router-dom'
import { decodeToken } from '../../shared/helpers/auth.helper'
import { AuthContext } from '../../shared/contexts/AuthContext'
import { ErrorModal } from '../../shared/components/sc-error-login/ErrorModal'
import Logo from '../../shared/img/Slogan.svg'

const LoginPage = () => {
  const { setLoading } = useContext(LoadingContext)
  const { setUser } = useContext(AuthContext)
  const [showErrorLogin, setShowErrorLogin] = useState(false)
  const history = useHistory()
  const schema = yup.object().shape({
    email: yup
      .string()
      .email('Es necesario un email')
      .required('Es necesario un email'),
    password: yup.string().required('Es necesario una contraseña')
  })
  const { handleSubmit, register, formState, setValue, control } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schema)
  })

  const onSubmit = async data => {
    setLoading(true)
    const token = await login(data)
      .catch(err => {
        console.log('Error login', err)
        setShowErrorLogin(true)
      })
      .finally(() => setLoading(false))
    if (token) {
      localStorage.setItem('token', token)
      const decoded = decodeToken()
      if (decoded.role !== 'ADMINISTRATOR') {
        setShowErrorLogin(true)
        localStorage.clear()
        return
      }
      setLoading(false)
      setUser(decoded)
      history.push('/home')
    }
  }

  return (
    <>
      {showErrorLogin && (
        <ErrorModal setShowErrorLogin={setShowErrorLogin}></ErrorModal>
      )}
      <div className={styles.container}>
        <div className={styles.content}>
          <form
            className={styles.content__form}
            onSubmit={handleSubmit(onSubmit)}
          >
            <div className={styles.content__form__logo}>
              <img src={Logo} alt='Logo'></img>
            </div>
            <div className={styles.content__form__label}>
              <span>Email*</span>
            </div>
            <input
              placeholder='Email'
              type='text'
              {...register('email')}
            ></input>
            <div className={styles.content__form__label}>
              <span>Contraseña*</span>
            </div>
            <input
              name='password'
              placeholder='Contraseña'
              type='password'
              {...register('password')}
            ></input>
            <input
              className={formState.isValid ? 'btn-enable' : 'btn-disable'}
              type='submit'
              value='Iniciar sesión'
            ></input>
          </form>
        </div>
      </div>
    </>
  )
}

export default LoginPage
