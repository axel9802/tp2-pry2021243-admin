import axios from 'axios';

export const login = async (login) => {
  let token: any = null;
  console.log(process.env.REACT_APP_PROXY_HOST);
  await axios
    .post(`${process.env.REACT_APP_PROXY_HOST}/api/users/login`, { email: login.email, password: login.password })
    .then((res) => {
      token = res.data.token;
    });

  return token;
};
