import React, { useContext, useEffect, useState } from 'react'
import Button from '@mui/material/Button'
import { Dialog, DialogTitle, DialogContent, DialogActions, Select, MenuItem, InputLabel,TextField } from '@mui/material';
import { getPolices } from '../endpoints/home.endpoints';
import { makeStyles } from '@mui/styles';
import { minWidth } from '@mui/system';
//import { LoadingContext } from '../../../shared/contexts/LoadingContext';

const useStyles = makeStyles((theme) => ({
    formControl: {
        width:'200px'
    }
}));

const AddPolice = (props) => {
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const [accident,setAccident] = useState({status:"",userPolice:"",})
    const [msg, setMsg] = useState('');
    //const { setLoading } = useContext(LoadingContext)
    const [polices, setPolices] = useState([]);
    const [police, setPolice] = useState('');
    const handleClickOpen = () => {
        setOpen(true);
    }
    const handleClose = () => {
        setOpen(false)
    }
    const handleSave = () => {
        console.log(props.params.data.id)
        console.log(accident)
        props.updateAccident(props.params.data.id,accident);
        handleClose()
    }
    useEffect(() => {
        //setLoading(true)
        getPolices()
            .then(res => {
                const resAdapter = res.map(x => ({
                    id: x?.id,
                    name: x?.name
                }))
                setPolices(resAdapter)
                
            })

    }, [])

    const updateaccident1 = () => {
        setAccident({
            status:'1',
            userPolice: police
        });
    }
    const handleChange = (event) => {
        setPolice(event.target.value);
        
    };
   



    return (
        <div>
            <Button style={{ marginBottom: 6}} variant="outlined" color="primary" onClick={handleClickOpen}>
                Asignar Policia
            </Button>

            <Dialog open={open} >
                <DialogTitle id="form-dialog-title">Seleciona tu policia</DialogTitle>
                <DialogContent>
                   <TextField
                       sx={{
                        '& .MuiTextField-root': { m: 1, width: '25ch' },
                      }}
                   className={classes.formControl}
                   select
                   label="Seleccione policia"
                   variant="filled"
                   value={police}
                   onChange={e=> handleChange(e)}
                   margin="dense"
                   >
                    {
                        polices.map((option)=>(
                          <MenuItem key={option.id} value={option.name}>
                              {option.name}
                              <script>{console.log(option.name)}</script>
                          </MenuItem>  
                        ))
                    }
                   </TextField>














                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cerrar
                    </Button>
                    <Button onClick={updateaccident1}>
                        Guardar
                    </Button>
                    <Button onClick={handleSave} color="primary">
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>


        </div>
    )
}

export default AddPolice