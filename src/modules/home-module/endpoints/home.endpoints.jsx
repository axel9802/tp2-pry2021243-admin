import axios from 'axios'

export const getAccidents = async () => {
  let accidents = []
  await axios
    .get(`${process.env.REACT_APP_PROXY_HOST}/api/accidents`)
    .then(res => {
      accidents = res.data
    })

  return accidents
}

export const deleteAccident = async id => {
  // let delete = []
  await axios.delete(`${process.env.REACT_APP_PROXY_HOST}/api/accidents/${id}`)
  
  // return accidents
}

export const getPolices = async () => {
  let polices = []
  await axios
    .get(`${process.env.REACT_APP_PROXY_HOST}/api/users/polices`)
    .then(res => {
      polices = res.data
    })
  console.log(polices)
  return polices
}

export const updatingAccident = async (id,data) => {
    await axios.put(`${process.env.REACT_APP_PROXY_HOST}/api/accidents/${id}`,data) .then(response => {
      console.log(response.status );
  })
}

