import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react'
import io from 'socket.io-client'
import shortid from 'shortid'
import { deleteAccident, getAccidents, updatingAccident } from './endpoints/home.endpoints'
import { AuthContext } from '../../shared/contexts/AuthContext'
import { SCNavbar } from '../../shared/components/sc-navbar/SCNavbar'
import style from './HomePage.module.scss'
import { AgGridColumn, AgGridReact } from 'ag-grid-react'
import { LoadingContext } from '../../shared/contexts/LoadingContext'
import AddPolice from './component/AddPolice'
import axios from 'axios'
const HomePage = () => {
  const { setLoading } = useContext(LoadingContext)
  const [socket, setSocket] = useState(null)
  const [accidents, setAccidents] = useState([])
  const { user } = useContext(AuthContext)
  const [gridApi, setGridApi] = useState(null)
  const [gridColumnApi, setGridColumnApi] = useState(null)
  const gridRef = useRef()
  const [enable, setEnable] = useState(false)

  useEffect(() => {
    setLoading(true)
    getAccidents()
      .then(res => {
        const resAdapter = res.map(x => ({
          ...x,
          status:getStatus(x?.status),
          owner: x?.owner ? x.owner : '-',
          phone: x?.phone ? x?.phone : '-',
          description: x?.description ? x.description : '-',
          conclusion: x?.conclusion ? x.conclusion : '-',
          dateCreated: x?.dateCreated ? x.dateCreated : '-',
          plate: x?.plate ? x.plate : '-',
          userPolice: x?.userPolice ? x.userPolice : '-'
        }))
        setAccidents(resAdapter)
      })
      .finally(() => setLoading(false))
    const newSocket = io(process.env.REACT_APP_HOST_SOCKET)
    setSocket(newSocket)
    // return () => newSocket.close()
  }, [])

  useEffect(() => {
    if (socket) {
      socket.on('accidents', data => {
        console.log(data)
        setAccidents(x => [data, ...x])
      })
    }
  }, [socket])

  function onGridReady(params) {
    setGridApi(params.api)
    setGridColumnApi(params.columnApi)
  }
  function getStatus(status){
    if (status == 0){
      return 'No atentido'
    } else if (status == 1){
      return 'En Proceso'
    } else if (status == 2){
      return 'Finalizado'
    } else {
      return '-'
    }
  }

  const defaultColDef = useMemo(() => {
    return {
      editable: false
    }
  }, [])

  const onRemoveSelected = useCallback(async () => {
    setLoading(true)
    const selectedData = gridRef.current.api.getSelectedRows()
    for (const data of selectedData) {
      await deleteAccident(data.id)
    }
    const res = gridRef.current.api.applyTransaction({ remove: selectedData })
    setLoading(false)
    // printResult(res)
  }, [])

  const updateAccident =  useCallback( async  (id, accident1) => {
    setLoading(true)
    const selectedData = gridRef.current.api.getSelectedRows()
    for (const data of selectedData) {
      await updatingAccident(data.id,accident1)
    }
    //console.log(`${process.env.REACT_APP_PROXY_HOST}/api/accidents/${id}`)
    getAccidents()
      .then(res => {
        const resAdapter = res.map(x => ({
          ...x,
          status: getStatus(x?.status),
          owner: x?.owner ? x.owner : '-',
          phone: x?.phone ? x?.phone : '-',
          description: x?.description ? x.description : '-',
          conclusion: x?.conclusion ? x.conclusion : '-',
          dateCreated: x?.dateCreated ? x.dateCreated : '-',
          plate: x?.plate ? x.plate : '-',
          userPolice: x?.userPolice ? x.userPolice : '-'
        }))
        setAccidents(resAdapter)
      })
      .finally(() => setLoading(false))
  },[])

  return (
    <>
      {user && <SCNavbar></SCNavbar>}
      <div className={`${style.container} limit-container`}>
        <h1>Control de accidentes</h1>
        <div className={style.container__buttons}>
          <button
            className={enable ? 'btn-enable' : 'btn-disable'}
            onClick={onRemoveSelected}
          >
            Remover
          </button>
        </div>
        <div className='center'>
          <div className='ag-theme-alpine' style={{ height: 550, width: 1250 }}>
            {/* <button onClick={() => changeView()}>Add customer</button> */}
            <AgGridReact
              onCellClicked={() => setEnable(true)}
              onGridReady={onGridReady}
              rowData={accidents}
              paginationPageSize={10}
              pagination={true}
              editType='fullRow'
              defaultColDef={defaultColDef}
              ref={gridRef}
              rowSelection={'multiple'}
            >
              <AgGridColumn headerName='Estado' field='status'></AgGridColumn>
              <AgGridColumn headerName='Placa' field='plate'></AgGridColumn>
              <AgGridColumn
                headerName='Propietario'
                field='owner'
              ></AgGridColumn>
              <AgGridColumn headerName='Telefono' field='phone'></AgGridColumn>
              <AgGridColumn headerName='SOAT' field='soat'></AgGridColumn>
              <AgGridColumn
                headerName='Fecha creación'
                field='dateCreated'
              ></AgGridColumn>
              <AgGridColumn headerName='Direccion' field='address'></AgGridColumn>
              <AgGridColumn headerName='ID' field='id'></AgGridColumn>

              <AgGridColumn
                headerName='Latitud'
                field='latitude'
              ></AgGridColumn>
              <AgGridColumn
                headerName='Altitud'
                field='longitude'
              ></AgGridColumn>
              <AgGridColumn
                headerName='Propietario'
                field='owner'
              ></AgGridColumn>
              <AgGridColumn
                headerName='Descripción'
                field='description'
              ></AgGridColumn>
              <AgGridColumn
                headerName='Conclusión'
                field='conclusion'
              ></AgGridColumn>
              <AgGridColumn
                headerName='Asignado'
                field='userPolice'
              >
              </AgGridColumn>
              
              
              
              <AgGridColumn field='Asignar' cellRendererFramework={(params) => {
                return (
                  <AddPolice updateAccident={updateAccident} params={params} >
                  </AddPolice>
                )
              }}></AgGridColumn>
            </AgGridReact>
          </div>
        </div>
      </div>
    </>
  )
}

export default HomePage
