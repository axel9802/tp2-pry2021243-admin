import React from 'react';
import { Container, createTheme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    root: {
      marginTop: `${theme.spacing(4)} 0`,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
  }));

const MainContainer = ({ children, ...props }) => {
    const styles = useStyles();
  
    return (
      <Container
        className={styles.root}
        component='main'
        maxWidth='xs'
        {...props}
      >
        {children}
      </Container>
    );
  };
  
  export default MainContainer;