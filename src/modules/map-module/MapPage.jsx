import React, {
    useContext, useEffect, useState
} from 'react'
import { AuthContext } from '../../shared/contexts/AuthContext'
import { SCNavbar } from '../../shared/components/sc-navbar/SCNavbar'
import { GoogleMap, Marker, InfoWindow } from "@react-google-maps/api";
import { useGoogleMaps } from "../../store/GoogleMapsProvider";
import { useHistory } from "react-router-dom";
import { useJsApiLoader } from "@react-google-maps/api";
import { LoadingContext } from '../../shared/contexts/LoadingContext'
import style from './MapPage.module.scss'
import { getAccidents } from './endpoints/map.endpoints';
import { makeStyles } from '@mui/styles';
import { Box, Container, Grid,Card, List,ListItem,ListItemIcon,ListItemText , Paper, Typography } from '@mui/material';

const center = { lat: -12.04799, lng: -77.05373 }
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    banner: {
        backgroundImage: 'linear-gradient(135deg, #f5f7fa 0%, #c3cfe2 100%)',
        height: '100vh',
    },

    timeline: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        color: '#fff',
    },

    padding: {
        background: '#131E60',
        paddingTop: '1rem',
        paddingBottom: '2rem',
        borderRadius: '20px',
        display: 'flex'
    },
    

    paddingDetails: {
        background: '#D50038',
        paddingTop: '1rem',
        marginBottom: '0.5rem',
        borderRadius: '20px',
        color: 'white',
        display: 'flex',
        justifyContent: 'space-between'
    },

    popup: {
        display: 'flex',
        flexDirection: 'column',
    },
    jss99: {
        margin: '64px',
    },
    container: {
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'column',
        marginBottom: '1rem'
    },

    resize: {
        width: '40px',
        marginRight: '1rem',
    },

    icon: {
        minWidth: '30px',
        color: '#fff'
    },

    end: {
        color: '#fff',
        textAlign: 'end',
        Text: '3px'
    },

    color2: {
        color: '#fff',
        fontSize: '65.2%'
    },

    justify: {
        justifyContent: 'space-between'
    },

    paddingStatus: {
        marginLeft: '52rem'
    },
    

    paddingDetail1: {
        paddingLeft: '2rem',
    },

    detailsData: {
        flexDirection: 'row',
        display: 'flex',
        color: '#fff'
    },

    paddingDetail2: {
        paddingRight: '4rem'
    },

    row: {
        flexDirection: 'row'
    },

    detail: {
        justifyContent: 'space-between',
        paddingLeft: '2rem',
    },

    detail2: {
        paddingRight: '2rem',
    }
}));
const MapPage = () => {
    const classes = useStyles();
    const { setLoading } = useContext(LoadingContext)
    const [map, setMap] = React.useState(null);
    const [markerMap, setMarkerMap] = useState({});
    const [selectedPlace, setSelectedPlace] = useState(null);
    const history = useHistory();
    const [infoOpen, setInfoOpen] = useState(false);
    const [accidents, setAccidents] = useState([])
    const [accident, setAccident] = useState(null)
    const { user } = useContext(AuthContext)
    const containerStyle = {
        height: '80vh', width: '100%',
        borderRadius: "4px",
    };
    const { isLoaded } = useJsApiLoader({
        id: "google-map-script",
        googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
    });



   // var size = new window.google.maps.Size(50, 50)
   /* const iconBase = {
        url: "https://i.imgur.com/FWwbDrT.png", // url
         scaledSize: new window.google.maps.Size(50, 50), // scaled size
         origin: new window.google.maps.Point(0, 0), // origin
         anchor: new window.google.maps.Point(0, 0) // anchor // size
    };*/

    const markerLoadHandler = (selected, marker) => {
        return setMarkerMap(prevState => {
            return { ...prevState, [marker.id]: selected };
        });
    };

    const markerClickHandler = (event, place) => {
        // Remember which place was clicked
        setSelectedPlace(place);
       setAccident(place)
        // Required so clicking a 2nd marker works as expected
        if (infoOpen) {
            setInfoOpen(false);
        }

        setInfoOpen(true);

        // If you want to zoom in a little on marker click
        if (zoom < 13) {
            setZoom(13);
        }
        
        console.log(police)
        // if you want to center the selected Marker
        //setCenter(place.pos)
    };





    useEffect(() => {
        setLoading(true)
        getAccidents()
            .then(res => {
                const resAdapter = res.map(x => ({
                    ...x,
                    id: x?.id,
                    lat: Number(x?.latitude),
                    long: Number(x?.longitude),
                    plate: x?.plate,
                    owner: x?.owner,
                    phone: x?.phone,
                    description: x?.description,
                    conclusion: x?.conclusion,
                    address: x?.address,
                    police: x?.userPolice,
                    status: getStatus(x?.status)
                }))
                setAccidents(resAdapter)
            })
            .finally(() => setLoading(false))
    }, [])

    function getStatus(status){
        if (status == 0){
          return 'No atentido'
        } else if (status == 1){
          return 'En Proceso'
        } else if (status == 2){
          return 'Finalizado'
        } else {
          return '-'
        }
      }


    return isLoaded ? (
        <>
            {user && <SCNavbar></SCNavbar>}


            <main className={`${style.formsito}`}>
                <Container maxWidth='lg'>
                    <Box m={8} />
                    <Grid container spacing={5}>

                        <Grid item xs={12} lg={11}>
                            {
                                !(infoOpen && selectedPlace) ? (
                                    <Box>
                                    </Box>
                                ) : (
                                    <Grid>
                                        <Box className={classes.paddingDetails}>
                                            <h3 className={classes.detail}> Detalle: </h3>
                                            <p className={classes.detail2}>Estado: {accident.status}</p>
                                        </Box>
                                        <Box className={classes.padding}>
                                        <Grid container spacing={3}>
                                            <Grid item xs={12} className={classes.container}>
                                                <div className={classes.paddingDetail1}>
                                                    <div>
                                                    <text className={classes.detailsData}> Placa: {accident.plate}</text>
                                                    </div>
                                                    <div>
                                                        <text className={classes.detailsData}> Soat: {accident.soat}</text>
                                                    </div>
                                                    <div>
                                                        <text className={classes.detailsData}> Contacto de Emergencia: {accident.phone} </text> 
                                                    </div>
                                                </div>
                                                <div>
                                                    <text className={classes.detailsData}> Dirección: {accident.address}</text>
                                                    <span></span>
                                                    <text className={classes.detailsData}> Conclusión: </text>  <ListItemText className={classes.color2} primary={accident.conclusion} />
                                                </div>
                                                <div className={classes.paddingDetail2}>
                                                    <text className={classes.detailsData}> Policia: {accident.police} </text> <ListItemText className={classes.color2} />
                                                </div>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                    </Grid>
                                )
                            }

                        </Grid>
                        <Grid item xs={15} lg={11}>
                            <GoogleMap
                                center={center}
                                zoom={13}
                                mapContainerStyle={containerStyle}
                            >
                                {accidents.map((marker, index) => {
                                    return (
                                        <Marker
                                            key={marker.id}
                                            position={
                                                {
                                                    lat: marker.lat,
                                                    lng: marker.long,
                                                }
                                            }
                                            onLoad={selected => markerLoadHandler(selected, marker)}
                                            onClick={event => markerClickHandler(event, marker)}
                                            icon={{
                                                url: "https://i.imgur.com/FWwbDrT.png", // url
                                                scaledSize: new window.google.maps.Size(45, 45), // scaled size
                                                origin: new window.google.maps.Point(0, 0), // origin
                                                anchor: new window.google.maps.Point(0,0) // anchor // size
                                              }}
                                        >
                                        </Marker>
                                    );
                                })}
                                {infoOpen && selectedPlace && (
                                    <InfoWindow
                                        anchor={markerMap[selectedPlace.id]}
                                        onCloseClick={() => setInfoOpen(false)}
                                    >
                                        <div>
                                            <h3>{selectedPlace.plate}</h3>
                                            <h4>{selectedPlace.owner}</h4>
                                            <h5>{selectedPlace.phone}</h5>
                                        </div>
                                    </InfoWindow>
                                )}
                            </GoogleMap>
                        </Grid>
                    </Grid>
                </Container>
            </main>

        </>


    ) : (
        <>
            {user && <SCNavbar></SCNavbar>}
            <div className={`${style.container} limit-container`}>
                <h1>Mapita Cargando ...</h1>
                <div className='center'>
                    <script>{console.log('rataaaaaaaaaaaaa')}</script>
                </div>
            </div>
        </>
    )
}

export default MapPage