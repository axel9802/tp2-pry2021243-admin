import React, {
    useContext, useEffect, useState
} from 'react'
import { AuthContext } from '../../shared/contexts/AuthContext'
import { SCNavbar } from '../../shared/components/sc-navbar/SCNavbar'
import { GoogleMap, Marker, InfoWindow } from "@react-google-maps/api";
import { useGoogleMaps } from "../../store/GoogleMapsProvider";
import { useHistory } from "react-router-dom";
import { useJsApiLoader } from "@react-google-maps/api";
import { LoadingContext } from '../../shared/contexts/LoadingContext'
import style from './MapPage.module.scss'
import { getAccidents } from './endpoints/map.endpoints';
import { makeStyles } from '@mui/styles';
import { Box, Container, Grid,Card, List,ListItem,ListItemIcon,ListItemText , Paper, Typography } from '@mui/material';
import  {People,AddLocationAlt,PhoneAndroid,Description,AssignmentTurnedIn }from '@mui/icons-material'

const center = { lat: -12.18994612, lng: -76.99423495 }
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    banner: {
        backgroundImage: 'linear-gradient(135deg, #f5f7fa 0%, #c3cfe2 100%)',
        height: '100vh',
    },

    timeline: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        color: '#fff',
    },

    padding: {
        background: '#6c63ff',
        paddingTop: '1rem',
        paddingBottom: '3rem',
        borderRadius: '20px',
    },
    popup: {
        display: 'flex',
        flexDirection: 'column',
    },
    jss99: {
        margin: '64px',
    },
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },

    resize: {
        width: '40px',
        marginRight: '1rem',
    },

    icon: {
        minWidth: '30px',
        color: '#fff'
    },

    card: {
        display: 'flex',
        flexDirection: 'row',
        overflow: 'hidden',
        padding: '1.5rem',
        color: '#fff',
        background: ' rgba( 255, 255, 255, 0.3 )',
        boxShadow: '0 8px 32px 0 rgba( 31, 38, 135, 0.37 )',
        backdropFilter: 'blur( 5px )',
        borderRadius: '20px',
        border: '1px solid rgba( 255, 255, 255, 0.18 )',
    },
}));
const MapPage = () => {
    const classes = useStyles();
    const { setLoading } = useContext(LoadingContext)
    const [map, setMap] = React.useState(null);
    const [markerMap, setMarkerMap] = useState({});
    const [selectedPlace, setSelectedPlace] = useState(null);
    const history = useHistory();
    const [infoOpen, setInfoOpen] = useState(false);
    const [accidents, setAccidents] = useState([])
    const [accident, setAccident] = useState(null)
    const { user } = useContext(AuthContext)
    const containerStyle = {
        height: '80vh', width: '100%',
        borderRadius: "4px",
    };
    const { isLoaded } = useJsApiLoader({
        id: "google-map-script",
        googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_API_KEY,
    });


    var size = new google.maps.Size(100, 100)
    const iconBase = {
        url: "https://i.imgur.com/FWwbDrT.png", // url
         scaledSize: size, // scaled size
         origin: new google.maps.Point(0, 0), // origin
         anchor: new google.maps.Point(0, 0) // anchor // size
    };

    const markerLoadHandler = (selected, marker) => {
        return setMarkerMap(prevState => {
            return { ...prevState, [marker.id]: selected };
        });
    };

    const markerClickHandler = (event, place) => {
        // Remember which place was clicked
        setSelectedPlace(place);
       setAccident(place)
        // Required so clicking a 2nd marker works as expected
        if (infoOpen) {
            setInfoOpen(false);
        }

        setInfoOpen(true);

        // If you want to zoom in a little on marker click
        if (zoom < 13) {
            setZoom(13);
        }

        // if you want to center the selected Marker
        //setCenter(place.pos)
    };





    useEffect(() => {
        setLoading(true)
        getAccidents()
            .then(res => {
                const resAdapter = res.map(x => ({
                    ...x,
                    id: x?.id,
                    lat: Number(x?.latitude),
                    long: Number(x?.longitude),
                    plate: x?.plate,
                    owner: x?.owner,
                    phone: x?.phone,
                    description: x?.description,
                    conclusion: x?.conclusion,
                    address: x?.address
                }))
                setAccidents(resAdapter)
            })
            .finally(() => setLoading(false))
    }, [])

    return isLoaded ? (
        <>
            {user && <SCNavbar></SCNavbar>}


            <main className={`${style.formsito}`}>
                <Container maxWidth='lg'>
                    <Box m={8} />
                    <Grid container spacing={5}>

                        <Grid item xs={8} lg={11}>
                            {
                                !(infoOpen && selectedPlace) ? (
                                    <Box>
                                    </Box>
                                ) : (
                                    <Box className={classes.padding}>
                                        <h2 className="text-2xl md:text-3xl text-center">
                                            Accidentes
                                        </h2>
                                        <Grid container spacing={3}>
                                            <Grid item xs={12} className={classes.container}>
                                                <List component='nav'>
                                                    <Card className={classes.card}>
                                                        <ListItem>
                                                            <ListItemIcon className={classes.icon}>
                                                                <People/>
                                                            </ListItemIcon>
                                                            <ListItemText primary={accident.owner} />
                                                        </ListItem>
                                                        <ListItem>
                                                            <ListItemIcon className={classes.icon}>
                                                                <AddLocationAlt />
                                                            </ListItemIcon>
                                                            <ListItemText primary={accident.address} />
                                                        </ListItem>
                                                        <ListItem>
                                                            <ListItemIcon className={classes.icon}>
                                                                <PhoneAndroid />
                                                            </ListItemIcon>
                                                            <ListItemText primary={accident.phone} />
                                                        </ListItem>
                                                        <ListItem>
                                                            <ListItemIcon className={classes.icon}>
                                                                <Description />
                                                            </ListItemIcon>
                                                            <ListItemText primary={accident.description} />
                                                        </ListItem>
                                                        <ListItem>
                                                            <ListItemIcon className={classes.icon}>
                                                                <AssignmentTurnedIn />
                                                            </ListItemIcon>
                                                            <ListItemText primary={accident.conclusion} />
                                                        </ListItem>
                                                    </Card>
                                                </List>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                )
                            }

                        </Grid>
                        <Grid item xs={15} lg={11}>
                            <GoogleMap
                                center={center}
                                zoom={13}
                                mapContainerStyle={containerStyle}
                            >
                                {accidents.map((marker, index) => {
                                    return (
                                        <Marker
                                            key={marker.id}
                                            position={
                                                {
                                                    lat: marker.lat,
                                                    lng: marker.long,
                                                }
                                            }
                                            onLoad={selected => markerLoadHandler(selected, marker)}
                                            onClick={event => markerClickHandler(event, marker)}
                                            icon={iconBase}
                                        >
                                        </Marker>
                                    );
                                })}
                                {infoOpen && selectedPlace && (
                                    <InfoWindow
                                        anchor={markerMap[selectedPlace.id]}
                                        onCloseClick={() => setInfoOpen(false)}
                                    >
                                        <div>
                                            <h3>{selectedPlace.plate}</h3>
                                            <h4>{selectedPlace.owner}</h4>
                                            <h5>{selectedPlace.phone}</h5>
                                        </div>
                                    </InfoWindow>
                                )}
                            </GoogleMap>
                        </Grid>
                    </Grid>
                </Container>
            </main>

        </>


    ) : (
        <>
            {user && <SCNavbar></SCNavbar>}
            <div className={`${style.container} limit-container`}>
                <h1>Mapita Cargando ...</h1>
                <div className='center'>
                    <script>{console.log('rataaaaaaaaaaaaa')}</script>
                </div>
            </div>
        </>
    )
}

export default MapPage