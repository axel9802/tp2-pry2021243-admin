import axios  from "axios";

export const getAccidents = async () => {
    let accidents = []
    await axios
      .get(`${process.env.REACT_APP_PROXY_HOST}/api/accidents`)
      .then(res => {
        accidents = res.data
      })
  
    return accidents
}