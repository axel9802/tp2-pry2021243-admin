import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react'
import shortid from 'shortid'
import { deleteUser, editUser, getUsers } from './endpoints/users.endpoints'
import { AuthContext } from '../../shared/contexts/AuthContext'
import { SCNavbar } from '../../shared/components/sc-navbar/SCNavbar'
import style from './UsersPage.module.scss'
import { AgGridColumn, AgGridReact } from 'ag-grid-react'
import { LoadingContext } from '../../shared/contexts/LoadingContext'

const UsersPage = () => {
  const { setLoading } = useContext(LoadingContext)
  const [socket, setSocket] = useState(null)
  const [users, setUsers] = useState([])
  const { user } = useContext(AuthContext)
  const [gridApi, setGridApi] = useState(null)
  const [gridColumnApi, setGridColumnApi] = useState(null)
  const gridRef = useRef()
  const [enable, setEnable] = useState(false)

  useEffect(() => {
    setLoading(true)
    getUsers()
      .then(res => {
        const resAdapter = res.map(x => ({
          ...x,
          dni: x?.dni ? x.dni : '-',
          name: x?.name ? x.name : '-',
          license: x?.license ? x.license : '-',
          patrolNumber: x?.patrolNumber ? x.patrolNumber : '-',
          dateOfBirth: x?.dateOfBirth ? x.dateOfBirth : '-',
          email: x?.email ? x.email : '-'
        }))
        setUsers(resAdapter)
      })
      .finally(() => setLoading(false))
  }, [])

  function onGridReady (params) {
    setGridApi(params.api)
    setGridColumnApi(params.columnApi)
  }

  const defaultColDef = useMemo(() => {
    return {
      editable: true
    }
  }, [])

  const onRemoveSelected = useCallback(async () => {
    setLoading(true)
    const selectedData = gridRef.current.api.getSelectedRows()
    for (const data of selectedData) {
      await deleteUser(data.id)
    }
    const res = gridRef.current.api.applyTransaction({ remove: selectedData })
    setLoading(false)
  }, [])

  const onEditSelected = async () => {
    setLoading(true)
    const selectedData = gridRef.current.api.getSelectedRows()
    for (const data of selectedData) {
      await editUser(data.id, data)
    }
    setLoading(false)
  }

  return (
    <>
      {user && <SCNavbar></SCNavbar>}
      <div className={`${style.container} limit-container`}>
        <h1>Control de usuarios</h1>
        <div className={style.container__buttons}>
          <button
            className={`${enable ? 'btn-enable' : 'btn-disable'} mr-10`}
            onClick={onEditSelected}
          >
            Editar
          </button>
          <button
            className={`${enable ? 'btn-enable' : 'btn-disable'}`}
            onClick={onRemoveSelected}
          >
            Remover
          </button>
        </div>
        <div className='center'>
          <div className='ag-theme-alpine' style={{ height: 550, width: 1250 }}>
            {/* <button onClick={() => changeView()}>Add customer</button> */}
            <AgGridReact
              onGridReady={onGridReady}
              rowData={users}
              paginationPageSize={10}
              pagination={true}
              editType='fullRow'
              defaultColDef={defaultColDef}
              ref={gridRef}
              rowSelection={'multiple'}
              onCellClicked={() => setEnable(true)}
            >
              <AgGridColumn headerName='ID' field='id'></AgGridColumn>
              <AgGridColumn headerName='DNI' field='dni'></AgGridColumn>
              <AgGridColumn headerName='Nombre' field='name'></AgGridColumn>
              <AgGridColumn headerName='CIP' field='license'></AgGridColumn>
              <AgGridColumn
                headerName='Número de placa'
                field='patrolNumber'
              ></AgGridColumn>
              <AgGridColumn
                headerName='Fecha de cumpleaños'
                field='dateOfBirth'
              ></AgGridColumn>
              <AgGridColumn
                headerName='Tipo de usuario'
                field='userType'
              ></AgGridColumn>
              <AgGridColumn headerName='Email' field='email'></AgGridColumn>
            </AgGridReact>
          </div>
        </div>
      </div>
    </>
  )
}

export default UsersPage
