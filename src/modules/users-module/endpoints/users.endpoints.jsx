import axios from 'axios'

export const getUsers = async () => {
  let users = []
  await axios.get(`${process.env.REACT_APP_PROXY_HOST}/api/users`).then(res => {
    users = res.data
  })

  return users
}

export const deleteUser = async id => {
  // let delete = []
  await axios.delete(`${process.env.REACT_APP_PROXY_HOST}/api/users/${id}`)
}

export const editUser = async (id, user) => {
  let edited = null
  await axios
    .put(`${process.env.REACT_APP_PROXY_HOST}/api/users/${id}`, user)
    .then(res => {
      edited = res.data
    })

  return edited
}
