import React, { Suspense, useState, lazy, useContext, useEffect } from 'react'
import logo from './logo.svg'
import './App.css'
import { HashRouter, Route } from 'react-router-dom'
import { Switch } from 'react-router-dom'
import { LoadingContext } from './shared/contexts/LoadingContext'
import { SCLoader } from './shared/components/sc-loader/SCLoader'
import './styles.scss'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-alpine.css'

const LoginLayout = lazy(() => import('./layouts/login-layout/LoginLayout'))
const HomeLayout = lazy(() => import('./layouts/home-layout/HomeLayout'))
const UsersLayout = lazy(() => import('./layouts/users-layout/UsersLayout'))
const MapLayout = lazy(()=> import('./layouts/map-layout/MapLayout'))

function App () {
  const { loading, setLoading } = useContext(LoadingContext)

  const Load = () => {
    useEffect(() => {
      setLoading(true)

      return () => setLoading(false)
    }, [])
    return <></>
  }

  return (
    <div>
      {loading && <SCLoader></SCLoader>}
      <HashRouter>
        <Suspense fallback={<Load></Load>}>
          <Switch>
            <Route exact path='/'>
              <LoginLayout></LoginLayout>
            </Route>
            <Route exact path='/home'>
              <HomeLayout></HomeLayout>
            </Route>
            <Route exact path='/users'>
              <UsersLayout></UsersLayout>
            </Route>
            <Route exact path='/map'>
             <MapLayout></MapLayout>
            </Route>
          </Switch>
        </Suspense>
      </HashRouter>
    </div>
  )
}

export default App