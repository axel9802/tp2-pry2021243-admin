import React, { useEffect } from 'react'
import { createContext } from 'react'
import { useState } from 'react'
import { decodeToken } from '../helpers/auth.helper'

export const AuthContext = createContext(null)

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null)
  const [currentTab, setCurrentTab] = useState(1)

  useEffect(() => {
    if (!user) {
      // localStorage.getItem('token')
      const decoded = decodeToken()
      setUser(decoded)
    }
  }, [])

  return (
    <AuthContext.Provider value={{ user, setUser, currentTab, setCurrentTab }}>
      {children}
    </AuthContext.Provider>
  )
}
