import React from "react";
import { motion } from "framer-motion";

const ModalAnimation = (props) => {
  const { children } = props;

  return (
    <motion.div
      initial={{ y: "-100vw" }}
      animate={{ y: "calc(100vw - 100vw)" }}
      transition={{ delay: 0.2 }}
    >
      {children}
    </motion.div>
  );
};

export { ModalAnimation };
