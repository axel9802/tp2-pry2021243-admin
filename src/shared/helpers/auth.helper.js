import decode from "jwt-decode";

export const decodeToken = () => {
  const token = localStorage.getItem("token");
  if (token) {
    const decoded = decode(token);
    return decoded;
  } else {
    return null;
  }
};
