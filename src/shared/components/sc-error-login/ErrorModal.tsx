import React, { useContext } from "react";
import styles from "./ErrorModal.module.scss";
import Error from "../../img/error.svg";
import { ModalAnimation } from "../../animations/ModalAnimation";
// import { ModalAnimation } from '@animations';

const ErrorModal = (props: any) => {
  const { setShowErrorLogin } = props;

  const close = () => {
    setShowErrorLogin(false);
  };

  const accept = () => {
    setShowErrorLogin(false);
  };

  return (
    <div className={styles.container}>
      <ModalAnimation>
        <div
          className={`${styles["container__content"]} flex-wrap flex-x-center`}
        >
          <div className={styles["container__content-img"]}>
            <img src={Error}></img>
          </div>
          <h1 className="text-center">Error de login</h1>
          <p className="text-center">
            Se encuentran las credenciales incorrectas o servidor caido.
          </p>
          <div
            className={`${styles["container__content__buttons"]} flex-center mt-15`}
          >
            <button className="btn-enable" onClick={() => accept()}>
              Aceptar
            </button>
          </div>
          <span
            className={styles["container__content-close"]}
            onClick={() => close()}
          >
            &times;
          </span>
        </div>
      </ModalAnimation>
    </div>
  );
};

export { ErrorModal };
