import React from "react";
import style from "./SCLoader.module.scss";

const SCLoader = (props) => {
  const { title = "Cargando . . ." } = props;

  return (
    <>
      <div className={style.container}>
        <div className={style.container__content}>
          <div className={style.container__content__loading}>
            <p className={style["container__content__loading-title"]}>
              {title}
            </p>
            <div
              className={`${style["container__content__loading-spinner"]} rotate`}
            >
              <svg
                enableBackground="new 0 0 200 200"
                viewBox="0 0 200 200"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="m106.3 189.8c11.6-.6 23.1-3.5 33.7-8.6s20.2-12.2 28.1-20.8c8-8.6 14.2-18.8 18.4-29.9 4.1-11 6.2-22.9 5.9-34.7-.3-11.9-2.8-23.6-7.6-34.5-4.7-10.9-11.5-20.9-20-29.3s-18.6-15.1-29.6-19.7-22.9-7-35-7.1c-12-.1-23.9 2.1-35.2 6.6-11.2 4.5-21.5 11-30.2 19.3s-15.8 18.3-20.9 29.4c-5.1 10.9-7.9 23-8.4 35.2s1.3 24.4 5.4 36c4 11.5 10.4 22.2 18.6 31.4 8.1 9.1 18 16.7 29 22.2s23 8.8 35.3 9.7c-12.3-.7-24.4-3.7-35.6-9.1-11.1-5.3-21.3-12.8-29.6-22-8.4-9.1-15.1-19.9-19.4-31.5-4.4-11.8-6.5-24.4-6.2-36.9s3-24.9 8-36.5c5.1-11.4 12.2-22 21.2-30.8 8.9-8.8 19.6-15.9 31.2-20.8 11.7-4.8 24.3-7.3 36.8-7.4 12.6-.1 25.3 2.2 37.1 6.9s22.6 11.7 31.8 20.5 16.8 19.4 22 31c5.3 11.7 8.3 24.3 8.8 37.1s-1.4 25.7-5.7 37.9-11 23.4-19.5 33c-8.6 9.6-19 17.6-30.5 23.4s-24.2 9.2-37.2 10.2c-2.8.2-5.3-1.9-5.5-4.7-.2-2.9 1.9-5.3 4.8-5.5z"
                  fill="#3a86ff"
                />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export { SCLoader };
