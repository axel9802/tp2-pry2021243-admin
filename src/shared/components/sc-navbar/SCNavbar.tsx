import React from "react";
import style from "./SCNavbar.module.scss";
import { useContext } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import { useHistory } from "react-router-dom";
import { SCLoader } from "../sc-loader/SCLoader";
import Close from "../../img/close.svg";
import Logo from "../../img/mini_logo.svg";
import { useEffect } from "react";

const SCNavbar = () => {
  /*Variables */
  const history = useHistory();
  const { user, currentTab, setCurrentTab } = useContext(AuthContext);
  // const { setMessageBus } = useContext(MessageBusContext);

  /*Functions */
  const changeTab = (tabIndex: number, url: string) => {
    setCurrentTab(tabIndex);
    history.push(url);
  };

  const goToHome = () => {
    history.push("/home");
  };

  const goToLogin = () => {
    localStorage.clear();
    history.push("/");
  };

  /*Manage state */
  if (!user) {
    return <SCLoader></SCLoader>;
  }

  useEffect(() => {
    if (currentTab === 1) {
      history.push("/home");
    }
  }, []);

  return (
    <div className={style.container}>
      <div
        className={`${style.container__content} flex flex-space-between  limit-container`}
      >
        <div className={style["container__content-logo"]}>
          {/* <p>Logo</p> */}
          <img src={Logo} onClick={() => goToHome()}></img>
        </div>
        <div
          className={`${style.container__content__right} flex flex-y-center `}
        >
          <div className={style["container__content__right-options"]}>
            <ul>
              <li
                className={currentTab === 1 ? "tab-activated" : ""}
                onClick={() => changeTab(1, "/home")}
              >
                Accidentes
              </li>
              <li
                className={currentTab === 2 ? "tab-activated" : ""}
                onClick={() => changeTab(2, "/users")}
              >
                Usuarios
              </li>
              <li
              className={currentTab === 3 ? "tab-activated" : ""}
              onClick={() => changeTab(3,"/map")}
              >
              Mapa
              </li>
              <li>
                <img src={Close} onClick={() => goToLogin()}></img>
              </li>
            </ul>
          </div>
          <div className={style["container__content__right-settings"]}>
            {/* <img src={User}></img> */}
          </div>
        </div>
      </div>
    </div>
  );
};

export { SCNavbar };