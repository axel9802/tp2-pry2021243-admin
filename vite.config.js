import { Alias, defineConfig, loadEnv } from "vite";
import path from "path";
import react from "@vitejs/plugin-react";
import * as jsconfig from "./jsconfig.paths.json";

function readAliasFromTsConfig() {
  const pathReplaceRegex = new RegExp(/\/\*$/, "");
  return Object.entries(jsconfig.compilerOptions.paths).reduce(
    (aliases, [fromPaths, toPaths]) => {
      const find = fromPaths.replace(pathReplaceRegex, "");
      const toPath = toPaths[0].replace(pathReplaceRegex, "");
      const replacement = path.resolve(__dirname, toPath);
      aliases.push({ find, replacement });
      return aliases;
    },
    []
  );
}

export default ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  console.log(process.env);

  return defineConfig({
    // plugins: [reactRefresh()],
    base: '/',
    plugins: [react()],
    resolve: {
      alias: readAliasFromTsConfig(),
    },
    define: {
      "process.env": process.env,
    },
    server: {
      port: 3600
    },
    // server: {
    //   port: Number(process.env.REACT_PORT),
    //   proxy: {
    //     "/projects": "http://localhost:3080",
    //     "/v1/dependencies": process.env.REACT_APP_PROXY_HOST,
    //     "/v1/auth/login": process.env.REACT_APP_PROXY_HOST,
    //     "/v1/wireframes": process.env.REACT_APP_PROXY_HOST,
    //     "/v1/projects": process.env.REACT_APP_PROXY_HOST,
    //     "/v1/users": process.env.REACT_APP_PROXY_HOST,
    //   },
    // },
  });
};
